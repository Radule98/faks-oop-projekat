package com.company;

public class Korisnik {

    public String ime, prezime, jmbg, username, password, brTelefona, drzavljanstvo;
    public Uloga uloga;

    // Sluzbenik
    Korisnik(String username, String password, String ime, String prezime, String jmbg, Uloga uloga){
        this.username = username;
        this.password = password;
        this.ime = ime;
        this.prezime = prezime;
        this.jmbg = jmbg;
        this.uloga = uloga;
    }

    // Iznajmljivac
    Korisnik (String username, String password, String ime, String prezime, String jmbg, Uloga uloga, String brTelefona, String drzavljanstvo){
        this.username = username;
        this.password = password;
        this.ime = ime;
        this.prezime = prezime;
        this.jmbg = jmbg;
        this.brTelefona = brTelefona;
        this.drzavljanstvo = drzavljanstvo;
        this.uloga = uloga;
    }

    public static boolean Exists(String username){
        for(int i = 0; i < TempData.korisnici.length; i++)
            if (TempData.korisnici[i].username.equals(username))
                return true;
        return false;
    }

    public static Korisnik Login(String username, String password){
        for(int i = 0; i < TempData.korisnici.length; i++)
            if(TempData.korisnici[i].username.equals(username) && TempData.korisnici[i].password.equals(password))
                return TempData.korisnici[i];
        return null;
    }
}
