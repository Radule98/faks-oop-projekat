package com.company;

import java.util.Date;

public class Vozilo {

    public String regBroj;
    public float potrosnjaGoriva, predjenoKM, kmDoServisiranja, cenaServisiranja, cenaIznajmljivanja, brSedista, brVrata;
    public Boolean obrisano;
    public Gorivo gorivo;
    public Servisiranje[] servisiranja;

    Vozilo(String regBroj, Boolean obrisano, Gorivo gorivo, Servisiranje[] servisiranja, float potrosnjaGoriva, float predjenoKM, float kmDoServisiranja, float cenaServisiranja, float cenaIznajmljivanja, float brSedista, float brVrata){
        this.regBroj = regBroj;
        this.obrisano = obrisano;
        this.gorivo = gorivo;
        this.servisiranja = servisiranja;
        this.potrosnjaGoriva = potrosnjaGoriva;
        this.predjenoKM = predjenoKM;
        this.kmDoServisiranja = kmDoServisiranja;
        this.cenaServisiranja = cenaServisiranja;
        this.cenaIznajmljivanja = cenaIznajmljivanja;
        this.brSedista = brSedista;
        this.brVrata = brVrata;
    }

    public static Vozilo[] DostupnaVozilaVremenski(Date pocetniDatum, Date krajnjiDatum)
    {

    }
}
