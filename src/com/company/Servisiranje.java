package com.company;
import java.util.Date;

public class Servisiranje {

    public String vozilo;
    public Date datum;
    public float predjeniKilometri;

    Servisiranje(String vozilo, Date datum, float predjeniKilometri){
        this.vozilo = vozilo;
        this.datum = datum;
        this.predjeniKilometri = predjeniKilometri;
    }
}
