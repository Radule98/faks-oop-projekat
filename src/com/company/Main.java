package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

    private static Korisnik trenutniKorisnik;

    public static void main(String[] args) {
        if(trenutniKorisnik == null){
            Prijava();
        }
    }

    private static void Prijava() {
        boolean userFound = false; boolean passwordCorrect = false;
        String username = null; String password;
        Scanner sc = new Scanner(System.in);

        while(!userFound){
            System.out.println("Unesite korisnicko ime");
            username = sc.nextLine();

            if(Korisnik.Exists(username))
                userFound = true;
            else
                System.out.println("Korisnik ne postoji, pokusajte ponovo.");
        }

        Helpers.clearConsole();

        while(!passwordCorrect){
            System.out.println("Unesite sifru");
            password = sc.nextLine();

            trenutniKorisnik = Korisnik.Login(username, password);

            if(trenutniKorisnik == null)
                System.out.println("Netacna sifra, pokusajte ponovo.");
            else
                passwordCorrect = true;
        }
    }

    private static void MainMenu(){
        Helpers.clearConsole();
        System.out.println("Dobrodosli " + trenutniKorisnik.ime + " " + trenutniKorisnik.prezime);
        System.out.println("Izaberite opciju");
        System.out.println("1) Trazenje vozila");
        // add more
        Scanner sc = new Scanner(System.in);
        int opcija = Integer.parseInt(sc.nextLine());

        switch(opcija){
            case 1:
                TrazenjeVozila();
            default:
                MainMenu();
        }
    }

    private static void TrazenjeVozila(){
        Helpers.clearConsole();
        System.out.println("Unesite datum kada zelite da iznajmite vozilo. Format: dd/MM/yyyy");

        Scanner sc = new Scanner(System.in);
        String unos = sc.nextLine();
        Date datumOdlaska; Date datumDolaska;
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        try{
            datumOdlaska = format.parse(unos);
        } catch(ParseException e){
            System.out.println("Pogresan format!");
        }

        System.out.println("Unesite datum kada zelite da vratite vozilo. Format: dd/MM/yyyy");
        unos = sc.nextLine();

        try{
            datumDolaska = format.parse(unos);
        } catch(ParseException e){
            System.out.println("Pogresan format!");
        }
    }
}
