package com.company;

public class PutnickoVozilo extends Vozilo {
    PutnickoVozilo(String regBroj, Boolean obrisano, Gorivo gorivo, Servisiranje[] servisiranja, float potrosnjaGoriva, float predjenoKM, float kmDoServisiranja, float cenaServisiranja, float cenaIznajmljivanja, float brSedista, float brVrata){
        super(regBroj, obrisano, gorivo, servisiranja, potrosnjaGoriva, predjenoKM, kmDoServisiranja, cenaServisiranja, cenaIznajmljivanja, brSedista, brVrata);
    }
}
